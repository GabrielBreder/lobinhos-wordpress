<?php
// Template Name: AboutUs
?>
<!DOCTYPE html>
<html lang="pt_br">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/styles/reset.css" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/styles/components/header.css" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/styles/components/footer.css" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/styles/components/button.css" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/styles/pages/about_us.css" />
    <title><?php bloginfo('name') ?> </title>
  </head>
  <body>
  <?php get_header(); ?>

    <section>
      <div class="main-container">
        <h1><?php the_field('titulo') ?></h1>
        <p>
          <?php the_field('descricao') ?>
        </p>
      </div>
    </section>

    <?php get_footer(); ?>

    <script src="/scripts/style.js"></script>
    <script src="/routes/routes.js" type="module"></script>
  </body>
</html>
