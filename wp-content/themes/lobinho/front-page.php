<?php
// Template Name: home
?>
<!DOCTYPE html>
<html lang="pt_br">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="shortcut icon" href="#" type="image/x-icon" />

    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/styles/reset.css" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/styles/components/header.css" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/styles/components/footer.css" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/styles/components/wolf_container.css" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/styles/components/button.css" />
    <title><?php bloginfo('name') ?> </title>
  </head>
  <body>
    <?php get_header(); ?>

    <section class="main-container">
      <div class="center">
        <h1><?php the_field('titulo') ?></h1>
        <div class="body-underline"></div>
        <div class="txt">
          <p>
            <?php the_field('descricao_titulo') ?>
          </p>
        </div>
      </div>
    </section>

    <section class="about-container">
      <h2><?php the_field('sobre') ?></h2>
      <p>
        <?php the_field('descricao_sobre') ?>
      </p>
    </section>

    <section class="value-container">
      <h2><?php the_field('valores') ?></h2>
      <div class="cards">
        <div class="protection card">
          <div class="img">
            <?php if( get_field('valor_1_imagem') ): ?>
                <img src="<?php the_field('valor_1_imagem') ?>" />
            <?php endif; ?>
          </div>
          <h3><?php the_field('valor_1_titulo') ?></h3>
          <p>
            <?php the_field('valor_1_descricao') ?>l.
          </p>
        </div>
        <div class="affection card">
          <div class="img">
            <?php if( get_field('valor_2_imagem') ): ?>
                <img src="<?php the_field('valor_2_imagem') ?>" />
            <?php endif; ?>
          </div>
          <h3><?php the_field('valor_2_titulo') ?></h3>
          <p>
            <?php the_field('valor_2_descricao') ?>
          </p>
        </div>
        <div class="companionship card">
          <div class="img">
            <?php if( get_field('valor_3_imagem') ): ?>
                <img src="<?php the_field('valor_3_imagem') ?>" />
            <?php endif; ?>
          </div>
          <h3><?php the_field('valor_3_titulo') ?></h3>
          <p>
            <?php the_field('valor_3_descricao') ?>
          </p>
        </div>
        <div class="rescue card">
          <div class="img">
            <?php if( get_field('valor_4_imagem') ): ?>
                <img src="<?php the_field('valor_4_imagem') ?>" />
            <?php endif; ?>
          </div>
          <h3><?php the_field('valor_4_titulo') ?></h3>
          <p>
          <?php the_field('valor_4_descricao') ?>
          </p>
        </div>
      </div>
    </section>

    <section class="example-container">
      <h2><?php the_field('secao_exemplos') ?></h2>
      <div class="wolf-container">
        <div class="wolf-img-container">
          <div class="wolf-background"></div>
          <div class="wolf-img">
          <?php if( get_field('imagem_lobo_1') ): ?>
                <img src="<?php the_field('imagem_lobo_1') ?>" />
            <?php endif; ?>
          </div>
        </div>
        <div class="wolf-description">
          <div class="wolf-header">
            <h3><?php the_field('nome_lobo_1') ?></h3>
            <p>Idade: <?php the_field('idade_lobo_1') ?> anos</p>
          </div>
          <div class="wolf-body">
            <p>
              <?php the_field('descricao_lobo_1') ?>
            </p>
          </div>
        </div>
      </div>
      <div class="wolf-container">
        <div class="wolf-img-container">
          <div class="wolf-background"></div>
          <div class="wolf-img">
            <?php if( get_field('imagem_lobo_2') ): ?>
                <img src="<?php the_field('imagem_lobo_2') ?>" />
            <?php endif; ?>
            
          </div>
        </div>
        <div class="wolf-description">
          <div class="wolf-header">
            <h3><?php the_field('nome_lobo_2') ?></h3>
            <p>Idade: <?php the_field('idade_lobo_2') ?> anos</p>
          </div>
          <div class="wolf-body">
            <p>
              <?php the_field('descricao_lobo_2') ?>
            </p>
          </div>
        </div>

    </section>

    <?php get_footer(); ?>

    <script src="/scripts/style.js"></script>
    <script src="/routes/routes.js" type="module"></script>
    <script src="/scripts/index/show_wolves.js" type="module"></script>
    <script src="/scripts/index/get_wolves.js" type="module"></script>
  </body>
</html>
