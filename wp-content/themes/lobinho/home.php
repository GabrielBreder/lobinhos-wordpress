<?php
// Template Name: WolfList
?>
<!DOCTYPE html>
<html lang="pt_br">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/styles/reset.css" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/styles/components/header.css" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/styles/components/footer.css" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/styles/components/wolf_container.css" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/styles/components/button.css" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/styles/pages/wolf_list.css" />
    <title><?php bloginfo('name') ?> </title>
  </head>
  <body>
  <?php get_header(); ?>

   

    <section class="example-container">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <div class="wolf-container">
          <div class="wolf-img-container">
            <div class="wolf-background"></div>
            <div class="wolf-img">
              <?php if( get_field('foto_do_lobo') ): ?>
                <img src="<?php the_field('foto_do_lobo'); ?>" />
              <?php endif; ?>

            </div>
          </div>
          <div class="wolf-description">
            <div class="wolf-header">
              <h3><?php the_field('nome_do_lobo'); ?></h3>
              <p><?php the_field('idade'); ?></p>
            </div>
            <div class="wolf-body">
              <p>
                <?php the_field('descricao'); ?>
              </p>
            </div>
          </div>
        </div>
        <?php endwhile; else: ?>
          <p>desculpe, o post não segue os critérios escolhidos</p>
        <?php endif; ?>
    </section>

    <?php my_pagination(); ?>
    <?php get_footer(); ?>

  </body>
</html>
