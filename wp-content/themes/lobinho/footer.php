<?php wp_footer();?>
<footer>
      <div class="footer">
        <div class="map">
          <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14701.399403880656!2d-43.1389299194178!3d-22.900463392564642!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9983d71a922e81%3A0x4fa7e811241855c6!2sS%C3%A3o%20Domingos%2C%20Niter%C3%B3i%20-%20RJ!5e0!3m2!1spt-BR!2sbr!4v1645307932405!5m2!1spt-BR!2sbr"
            width="300"
            height="300"
            style="border: 0"
            allowfullscreen=""
            loading="lazy"
          ></iframe>
        </div>
        <div class="details">
          <div class="address">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/icons/local-icon.svg" alt="location icon" />
            <p>
              Av. Milton Tavares de Souza, s/n - Sala 115 B - Boa Viagem,
              Niterói - RJ,24210-315
            </p>
          </div>
          <div class="phone">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/icons/phone-icon.svg" alt="phone icon" />
            <p>(99) 99999-9999</p>
          </div>
          <div class="email">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/icons/email-icon.svg" alt="email icon" />
            <p>salve-lobos@lobINhos.com</p>
          </div>
          <div class="btn" id="footer-btn">
            <p>Quem Somos</p>
          </div>
        </div>
        <div class="paws">
          <p>Desenvolvido com</p>
          <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/paws.png" alt="paws icon" />
        </div>
      </div>
    </footer>
     