<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'HQWRWh7NoNcHPC6UMnFBveRJJ6xfjFy+gD6iMmOYx6nGuf4P1/EAca0v0psYNrO3lVdfKyLRVmwCDS4PZhB3FA==');
define('SECURE_AUTH_KEY',  'Ypush61xfy2a7Bluj116+drT8odkWV+2Bbg38oxGBHLD9FDcVHgOFT02SfUrDXGKx1o3y4RqlCdOT4WLGFlLcA==');
define('LOGGED_IN_KEY',    'Khey1Y8nQx6U9zzL3HIw9+DrIcdUuOI7nWr8cjeUGQQGYFUWd7Zb3A4OF/75P8/K3CfuLis5A1bV43OrOCDrDg==');
define('NONCE_KEY',        '8kYLVP4cE7wHp6xMkhgedtIWKySF9S0COscAL6isDCaSzx1cwSxe62ruRKg0NgRbzLtU/X3aTCw9Si7uSFe5AQ==');
define('AUTH_SALT',        '6DG+JiWbwrivlEnc9CDks/QDxcunPV0lFzYf6I04j2dwgOKCjU0OhWXVa/+gAExRxFe41jQazwb7dzl9d54bHQ==');
define('SECURE_AUTH_SALT', 'sB/muJz4qo+q+mGSsFHF4RV9yqm5z3DXKSWPJrUhZR9Ry1zCyEkEqkzUGfBDTBYCiTtKFZIZSf7deTg9NBl0FA==');
define('LOGGED_IN_SALT',   'RWztLZWf0ExNL+20QuKbhdauRbAmZ+Z4vaZC/zZEEdUZXI+H0fVjoAW3v1Qx0Ckbe1TdWroaz+BbVzqcE/e5vQ==');
define('NONCE_SALT',       'Sh6EtFL+1Zy+DZkN2E33SIiEv0lJmLZRg/0tYRlZPB9xc2/CL2SfddsWTUNtGVkAVJkxDq+bc9pe12XLOFyFSg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
